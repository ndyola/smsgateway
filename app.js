const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const http = require("http");
const path = require("path");
const exphbs = require("express-handlebars");
const passport = require("passport");
const flash = require("connect-flash");
const session = require("express-session");
const moment = require("moment");

// Passport Config
require("./config/passport")(passport);

// Set up the express app
const app = express();
// Log requests to the console.
app.use(logger("dev"));
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Express session
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  next();
});

//Express handlebars
app.set("views", path.join(__dirname, "views"));
app.engine(
  "handlebars",
  exphbs({
    defaultLayout: "main",
    helpers: {
      toJSON: function(object) {
        return JSON.stringify(object);
      },
      formatDate: function(date, format) {
        return moment(date).format(format);
      },
      if_eq: function(a, b, opts) {
        if (a == b) {
          return opts.fn(this);
        } else {
          return opts.inverse(this);
        }
      }
    }
  })
);
app.set("view engine", "handlebars");

// Models
var models = require("./models");

models.sequelize.sync().then(function() {
  console.log("Nice Database looks fine!!!");
});

require("./routes")(app); // For API
require("./routes/handleBarView")(app); // For Express HandleBar View
const port = parseInt(process.env.PORT, 10) || 3000;
app.set("port", port);
const server = http.createServer(app);
server.listen(port, () =>
  console.log(`Server is running at port no : ${port}`)
);
module.exports = app;
