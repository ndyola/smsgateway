const User = require("../models").User;
const Device = require("../models").Device;
const Message = require("../models").Message;
const Role = require("../models").Role;
const ip = require("ip");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { forwardAuthenticated, ensureAuthenticated } = require("../config/auth");

const passport = require("passport");
// require("../config/passport")(passport);

module.exports = app => {
  /** Get Login Page */

  // Login
  app.post("/users/login", (req, res, next) => {
    passport.authenticate("local", {
      successRedirect: "/dashboard",
      failureRedirect: "/",
      failureFlash: true
    })(req, res, next);
  });

  // Logout
  app.get("/users/logout", (req, res) => {
    req.logout();
    req.session.destroy();
    res.redirect("/");
  });

  app.get("/", (req, res) => {
    res.render("login", { layout: "login" });
  });

  /** Get Dashboard after login */
  app.get("/dashboard", ensureAuthenticated, (req, res) => {
    let Auth = req.user.role_id === 1 ? true : false;
    res.render("home", {
      Auth
    });
  });

  /** Get All Users Data */
  app.get("/users", ensureAuthenticated, async (req, res) => {
    let Auth = req.user.role_id === 1 ? true : false;
    if (req.user.role_id === 2) return false;
    const userId = req.user.dataValues.id;
    await User.findAll({
      where: { id: { [Op.not]: userId } },
      attributes: ["email", "status", "id", "role_id"],
      include: [
        {
          model: Device,
          as: "devices",
          attributes: ["id", "token", "device_ip", "createdAt"],
          include: [
            {
              model: Message,
              as: "deviceMessage"
            }
          ]
        },
        {
          model: Role,
          as: "roles",
          attributes: ["id", "name"]
        }
      ]
    })
      .then(users => {
        res.render("users", {
          title: "User List",
          users: users,
          Auth
        });
      })
      .catch(err => console.log("Error: " + err));
  });

  /** Get All Message of Users */
  app.get("/users/:id", ensureAuthenticated, async (req, res) => {
    let Auth = req.user.role_id === 1 ? true : false;
    if (req.user.role_id === 2) return false;
    const userId = req.params.id;
    if (userId) {
      await User.findAll({
        where: { id: userId },
        attributes: ["email", "status", "id"],
        include: [
          {
            model: Device,
            as: "devices",
            attributes: ["id", "token", "device_ip", "createdAt"],
            include: [
              {
                model: Message,
                as: "deviceMessage"
              }
            ]
          }
        ]
      })
        .then(users => {
          res.render("userMessages", {
            title: "Message List",
            users: users,
            Auth
          });
        })
        .catch(err => console.log("Error: " + err));
    }
  });

  /** Get All Message of Users */
  app.get("/messages", ensureAuthenticated, async (req, res) => {
    let Auth = req.user.role_id === 1 ? true : false;
    if (req.user.role_id === 1) return false;
    const userId = req.user.id;
    if (userId) {
      await User.findAll({
        where: { id: userId },
        attributes: ["email", "status", "id"],
        include: [
          {
            model: Device,
            as: "devices",
            attributes: ["id", "token", "device_ip", "createdAt"],
            include: [
              {
                model: Message,
                as: "deviceMessage"
              }
            ]
          }
        ]
      })
        .then(users => {
          res.render("userMessages", {
            title: "Message List",
            users: users,
            Auth
          });
        })
        .catch(err => console.log("Error: " + err));
    }
  });
};
