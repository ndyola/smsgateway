const usercontroller = require("../controllers").user;
const messagescontroller = require("../controllers").messages;

const passport = require("passport");
require("../middleware/passport.middleware")(passport);

const middleware = function(req, res, next) {
  // requires auth
  passport.authenticate(
    "jwt",
    {
      session: false
    },
    function(err, user, info) {
      console.log("User", user);
      req.authenticated = !!user;
      req.user = user;
      if (!user) {
        return res.send("authentication required for this call");
      }
      console.log("User", user);
      next();
    }
  )(req, res, next);
};

const omiddleware = function(req, res, next) {
  // middleware with optional auth
  passport.authenticate(
    "jwt",
    {
      session: false
    },
    function(err, user, info) {
      req.authenticated = !!user;
      req.user = user;
      next();
    }
  )(req, res, next);
};

module.exports = app => {
  app.get("/api", (req, res) => {
    res.json({
      status: "success",
      message: "HT-SMS API",
      data: { version_number: "v1.0.0" }
    });
  });

  app.post("/api/users/create", usercontroller.create);
  app.post("/api/users/login", omiddleware, usercontroller.login);

  //Messages
  app.post("/api/messages/send", omiddleware, messagescontroller.send);
  app.get(
    "/api/messages/getReceivedData/:device_id",
    omiddleware,
    messagescontroller.getReceivedData
  );
  app.put("/api/messages/cancel", omiddleware, messagescontroller.cancel);
  app.get("/api/messages/:id", omiddleware, messagescontroller.index);
  app.get(
    "/api/messages/getMessage/:device_id",
    omiddleware,
    messagescontroller.getMessage
  );
  app.post(
    "/api/messages/sendInboxText",
    omiddleware,
    messagescontroller.sendInboxText
  );
};
