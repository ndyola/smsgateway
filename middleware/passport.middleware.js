const { ExtractJwt, Strategy } = require("passport-jwt");
const usercontroller = require("../controllers").user;
const config = require("../config/ht_sms");

module.exports = function(passport) {
  const opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = config.login.jwtEncryption;

  passport.use(
    new Strategy(opts, async function(jwtPayload, done) {
      usercontroller.getUser({ id: jwtPayload.user_id }, (err, user) => {
        console.log("USer", user);
        if (err) {
          return done(err, false);
        }
        if (user) {
          return done(null, user);
        } else {
          return done(null, false);
        }
      });
    })
  );
};
