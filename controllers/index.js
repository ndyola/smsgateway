const user = require("./user");
const messages = require("./messages");

module.exports = {
  user,
  messages
};
