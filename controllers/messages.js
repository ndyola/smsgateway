const User = require("../models").User;
const Device = require("../models").Device;
const Message = require("../models").Message;
const authService = require("../services/auth.services");
const validator = require("validator");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const uuid = require("uuid");
const moment = require("moment");

module.exports = {
  /**
   * Send Message From Application to Gateway
   * Send single/ multiple messages
   */
  async send(req, res, next) {
    let errors = [];
    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(200).send(errors);
    }

    const messages = req.body;
    let newData = [];
    const status = "pending";
    const log = `[{"status": "pending","occurred_at": "${moment().format()}"}]`;
    newData = messages.map(res => {
      var msg = Object.assign({}, res);
      msg.message_id = uuid();
      msg.status = status;
      msg.log = log;
      return msg;
    });
    await Message.bulkCreate(newData)
      .then(result => {
        let data = {};
        let newMessage = [];
        newMessage = newData.map(res => {
          var msg = Object.assign({}, res);
          msg.log = JSON.parse(res.log);
          return msg;
        });
        data.response = "success";
        data.messages = newMessage;
        res.status(201).send(data);
      })
      .catch(error => {
        let result = {
          response: "error",
          error
        };
        res.status(400).send(result);
      });
  },

  /**
   * Cancel Messages From Application to Gateway
   * Cancel single/ mulitple messages
   */
  async cancel(req, res, next) {
    let errors = [];
    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(200).send(errors);
    }

    const messages_ids = req.body;
    let message_id = [];
    message_id = messages_ids.map(res => {
      return res.message_id;
    });

    await Message.findAll({
      where: {
        message_id: { [Op.in]: message_id }
      },
      attributes: ["id", "message_id", "status", "log"]
    })
      .then(result => {
        return result.map(res => {
          var msg = Object.assign({}, res.dataValues);
          msg.status = "cancel";
          return msg;
        });
      })
      .then(data => {
        Message.bulkCreate(data, { updateOnDuplicate: ["id"] })
          .then(result => {
            res.status(201).send(result);
          })
          .catch(error => {
            res.status(400).send(error);
          });
      })
      .catch(error => {
        res.status(400).send(error);
      });
  },

  /**
   * Get Message From Gateway to App
   */
  async index(req, res, next) {
    let errors = [];
    if (!req.params.id) {
      errors.push({ msg: "Device id is empty" });
      return res.status(200).send(errors);
    }
    const device_id = req.params.id;
    await Message.findAll({
      where: {
        device_id: device_id,
        status: "pending"
      }
    })
      .then(result => {
        let data = {};
        data.response = "success";
        data.messages = result;
        res.status(201).send(data);
      })
      .catch(error => {
        let result = {
          response: "error",
          error
        };
        res.status(400).send(result);
      });
  },

  /**
   * Get All Sent Data
   */
  async getReceivedData(req, res, next) {
    let errors = [];
    if (!req.params.device_id) {
      errors.push({ msg: "Device id is empty" });
      return res.status(200).send(errors);
    }
    const device_id = req.params.device_id;
    await Message.findAll({
      where: {
        device_id: device_id,
        status: "sent"
      }
    })
      .then(result => {
        let data = {};
        data.response = "success";
        data.messages = result;
        res.status(201).send(data);
      })
      .catch(error => {
        let result = {
          response: "error",
          error
        };
        res.status(400).send(result);
      });
  },

  /**
   * Get Max message_id from Gateway
   */
  async getMessage(req, res, next) {
    let errors = [];
    if (!req.params.device_id) {
      errors.push({ msg: "Device id is empty" });
      return res.status(200).send(errors);
    }
    const device_id = req.params.device_id;
    await Message.findOne({
      attributes: [
        [Sequelize.fn("max", Sequelize.col("message_id")), "message_id"]
      ],
      raw: true,
      where: {
        device_id: device_id
      }
    })
      .then(result => {
        res.status(201).send(result);
      })
      .catch(error => {
        res.status(400).send(error);
      });
  },

  /**
   * sync data between app and gateway
   */
  async syncData(req, res, next) {},

  /**
   * Send Inbox Text of App to Gateway
   */
  async sendInboxText(req, res, next) {
    let errors = [];
    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(200).send(errors);
    }
    /** Validate DeviceID, Message and Phone Number */
    if (!req.body.device_id) {
      errors.push({ msg: "Please enter a valid device." });
    }

    if (errors.length > 0) return res.status(200).send(errors);

    // check the messages
    let flag = false;
    let messages = req.body.inbox_messages;
    for (var i in messages) {
      let newLogs = [];
      let log = JSON.parse(messages[i].log);
      newLogs.push(...log, {
        status: messages[i].status,
        occurred_at: moment().format()
      });
      if (messages[i].id === 0) {
        await Message.build({
          message_id: messages[i].message_id,
          phone_number: messages[i].phone_number,
          messages: messages[i].messages,
          device_id: req.body.device_id,
          status: messages[i].status,
          log: JSON.stringify(newLogs)
        })
          .save()
          .then(result => {
            if (result) {
              // true
              flag = true;
            } else {
              // false
              flag = false;
              return false;
            }
          });
      } else {
        await Message.update(
          { status: messages[i].status, log: JSON.stringify(newLogs) },
          { where: { id: messages[i].id } }
        ).then(res => {
          if (res[0] === 1) {
            // true
            flag = true;
          } else {
            // false
            flag = false;
            return false;
          }
        });
      }
    }

    res.status(201).send({ response: flag ? "success" : "error" });
  },

  /**
   * Update message from App to Gateway
   */
  async update(res, req, next) {
    let errors = [];
    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(200).send(errors);
    }
    /** Validate DeviceID, Message and Phone Number */
    if (!req.body.device_id) {
      errors.push({ msg: "Please enter a valid device." });
    }

    if (errors.length > 0) return res.status(200).send(errors);

    // // check the messages
    // let newData = [];
    // let messages = req.body.inbox_messages;
    // for (var i in messages) {
    //   newData.push({
    //     message_id: messages[i].message_id,
    //     phone_number: messages[i].phone_number,
    //     messages: messages[i].messages,
    //     device_id: req.body.device_id
    //   });
    // }
    // await Message.bulkCreate(newData)
    //   .then(result => {
    //     res.status(201).send(result);
    //   })
    //   .catch(error => {
    //     res.status(400).send(error);
    //   });
  }
};
