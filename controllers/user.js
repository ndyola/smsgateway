const User = require("../models").User;
const Device = require("../models").Device;
const Message = require("../models").Message;
const authService = require("../services/auth.services");
const validator = require("validator");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const base64url = require("base64url");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const ip = require("ip");

module.exports = {
  /**
   * Create/ Register User
   */
  async create(req, res, next) {
    let errors = [];

    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(400).send(errors);
    }

    /** Email and Password Validation */
    if (!validator.isEmail(req.body.email)) {
      errors.push({ msg: "Please enter a valid email." });
    }

    if (validator.isEmpty(req.body.password)) {
      errors.push({ msg: "Please enter a password." });
    }

    if (req.body.imeiNo === "") {
      errors.push({ msg: "Please contact with administrator." });
    }

    let error = {
      response: "error",
      errors: null
    };
    if (errors.length > 0) {
      error.errors = errors;
      return res.status(400).send(error);
    }

    /** check if email already exist or not */
    const { email, password, imeiNo } = req.body;
    await User.findOne({
      where: { email: email }
    }).then(user => {
      if (user) {
        errors.push({ msg: "Email already exists" });
        error.errors = errors;
        return res.status(200).send(error);
      } else {
        encryptPassword(password, (err, password) => {
          if (err) {
            return res.status(200).send(err);
          }

          User.build({
            email: email,
            password: password,
            status: 1,
            role_id: 2
          })
            .save()
            .then(result => {
              const user_id = result.id;
              const device = {
                imeiNo: imeiNo,
                user_id: user_id,
                device_ip: ip.address()
              };
              Device.build(device)
                .save()
                .then(data => {
                  if (data) {
                    let result = {
                      response: "success"
                    };
                    res.status(201).send(result);
                  }
                })
                .catch(error => {
                  let result = {
                    response: "error",
                    error
                  };
                  res.status(400).send(result);
                });
            })
            .catch(error => {
              let result = {
                response: "error",
                error
              };
              res.status(400).send(result);
            });
        });
      }
    });
  },

  /**
   * User Login
   */
  async login(req, res, next) {
    let errors = [];

    if (!req.is("application/json")) {
      errors.push({ msg: "Expects 'application/json'" });
      return res.status(400).send(errors);
    }

    /** Email and Password Validation */
    if (!validator.isEmail(req.body.email)) {
      errors.push({ msg: "Please enter a valid email." });
    }

    if (validator.isEmpty(req.body.password)) {
      errors.push({ msg: "Please enter a password." });
    }

    if (req.body.imeiNo === "") {
      errors.push({ msg: "Please contact with administrator." });
    }

    let error = {
      response: "error",
      errors: null
    };
    if (errors.length > 0) {
      error.errors = errors;
      return res.status(400).send(error);
    }

    await authService.authUser(req.body, (err, data) => {
      if (err) {
        errors.push({ msg: err });
        error.errors = errors;
        return res.status(200).send(error);
      }
      // no user
      if (data.result === false) {
        errors.push({ msg: "Please check email/password" });
        error.errors = errors;
        return res.status(200).send(error);
      }
      Device.update(
        { token: data.token },
        { where: { id: data.user.devices[0].id } }
      )
        .then(result => {
          let newData = {};
          if (result[0] === 1) {
            newData.response = "success";
            newData.token = data.token;
            newData.deviceId = data.user.devices[0].id;
            newData.userId = data.user.id;
            newData.email = data.user.email;
            newData.imeiNo = data.user.devices[0].imeiNo;
            newData.deviceIP = data.user.devices[0].device_ip;
            res.status(201).send(newData);
          } else {
            newData.response = "error";
            newData.error.push({ msg: "Please check email/password" });
            res.status(200).send(newData);
          }
        })
        .catch(error => {
          let result = {
            response: "error",
            error
          };
          res.status(400).send(result);
        });
    });
  }
};

/**
 * Functions
 */
function encryptPassword(plainPwd, callback) {
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return callback(err);
    }
    bcrypt.hash(plainPwd, salt, (err, password) => {
      if (err) {
        return callback(err);
      }
      callback(null, password);
    });
  });
}

function randomStringAsBase64Url(size) {
  return base64url(crypto.randomBytes(size));
}
