"use strict";

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const request = require("request");
const validator = require("validator");

const config = require("../config/ht_sms");
const usercontroller = require("../controllers").user;
const User = require("../models").User;
const Device = require("../models").Device;
const Message = require("../models").Message;

const getUniqueKeyFromBody = function(body) {
  // this is so they can send in 3 options unique_key, email, or phone and it will work
  let unique_key = body.unique_key;
  if (typeof unique_key === "undefined") {
    if (typeof body.email != "undefined") {
      unique_key = body.email;
    } else if (typeof body.phone != "undefined") {
      unique_key = body.phone;
    } else {
      unique_key = null;
    }
  }

  return unique_key;
};
module.exports.getUniqueKeyFromBody = getUniqueKeyFromBody;

const createUser = (userInfo, callback) => {
  let unique_key, auth_info, err;

  auth_info = {};
  auth_info.status = "create";

  unique_key = getUniqueKeyFromBody(userInfo);
  if (!unique_key) {
    return callback(new Error("An email or phone number was not entered."));
  }

  if (validator.isEmail(unique_key)) {
    auth_info.method = "email";
    userInfo.email = unique_key;

    usercontroller.create(userInfo, (err, user) => {
      if (err) {
        return callback(new Error("user already exists with that email"));
      }
      callback(null, user);
    });
  } else {
    callback(new Error("A valid email was not entered."));
  }
};
module.exports.createUser = createUser;

const authUser = function(userInfo, callback) {
  //returns token
  let unique_key;
  let auth_info = {};
  auth_info.status = "login";
  unique_key = getUniqueKeyFromBody(userInfo);
  if (!unique_key)
    return callback(
      new Error("Please enter an email or phone number to login")
    );

  if (!userInfo.password)
    return callback(new Error("Please enter a password to login"));

  if (!userInfo.imeiNo)
    return callback(new Error("Please contact administrator."));

  let user;

  if (validator.isEmail(unique_key)) {
    auth_info.method = "email";
    User.findOne({
      where: { email: unique_key },
      attributes: ["id", "email", "status", "password"],
      include: [
        {
          model: Device,
          as: "devices",
          where: { imeiNo: userInfo.imeiNo },
          attributes: ["id", "user_id", "imeiNo", "token", "device_ip"],
          include: [
            {
              model: Message,
              as: "deviceMessage"
            }
          ]
        }
      ]
    })
      .then(userDetail => {
        if (!userDetail) {
          return callback("Not registered");
        }
        const user = userDetail.dataValues;
        bcrypt.compare(userInfo.password, user.password, (err, same) => {
          if (err) {
            return callback(err);
          }
          if (!same) {
            return callback(null, { result: false });
          }
          const { token, expiration } = issueToken(user.id);
          callback(null, { result: true, token, expiration, user });
        });
      })
      .catch(err => console.log("Error: " + err));
  } else {
    return callback("A valid email was not entered");
  }
};
module.exports.authUser = authUser;

function issueToken(userId) {
  const expiration = parseInt(config.login.jwtExpiration);
  const token =
    "Bearer: " +
    jwt.sign({ user_id: userId }, config.login.jwtEncryption, {
      expiresIn: expiration
    });
  return { token, expiration };
}
