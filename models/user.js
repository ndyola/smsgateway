"use strict";
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      status: DataTypes.INTEGER,
      role_id: DataTypes.INTEGER
    },
    {}
  );
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.Device, {
      foreignKey: "user_id",
      as: "devices"
    });

    User.belongsTo(models.Role, {
      foreignKey: "role_id",
      as: "roles"
    });
  };
  return User;
};
