"use strict";
module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define(
    "Device",
    {
      user_id: DataTypes.INTEGER,
      imeiNo: DataTypes.BIGINT,
      device_ip: DataTypes.STRING,
      token: DataTypes.STRING
    },
    {}
  );
  Device.associate = function(models) {
    // associations can be defined here
    Device.hasMany(models.Message, {
      foreignKey: "device_id",
      as: "deviceMessage"
    });
  };
  return Device;
};
