"use strict";
module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define(
    "Message",
    {
      message_id: DataTypes.STRING,
      device_id: DataTypes.INTEGER,
      phone_number: DataTypes.STRING,
      messages: DataTypes.TEXT,
      status: DataTypes.STRING,
      log: DataTypes.STRING
    },
    {}
  );
  Message.associate = function(models) {
    // associations can be defined here
    Message.belongsTo(models.Device, {
      as: "usermessages",
      foreignKey: "device_id"
    });
  };
  return Message;
};
